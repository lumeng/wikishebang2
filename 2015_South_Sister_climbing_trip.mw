== Trip synopsis ==

* A synopsis of the 12 mile climb to the South Sister Summit: <http://www.everytrail.com/guide/south-sister-summit> (~15 min reading)

* Pictures of the view along the climb: <http://alltrails.com/api/alltrails/photos/10076886/image?size=extra_large&api_key=3p0t5s6b5g4g0e8k3c1j3w7y5c3m4t8i> (~5 min reading)

* summitpost.org, [http://www.summitpost.org/south-sister/150455 A sheet of general information about South Sister], http://www.summitpost.org/south-sister/150455.

* Oregon Public Broadcasting, [https://www.youtube.com/watch?v=9SQFO89TpsQ video showcasing Three Sisters Wilderness scenary] and [https://www.youtube.com/watch?v=ol3K4JJI_6M Cascade Range Mountain Peaks]

== Trip agenda ==

* Day 1 (a Friday)

Drive to [http://www.fs.usda.gov/recarea/deschutes/recarea/?recid=38850 Devil's Lake campground]. Camp at the campground for the night. See introduction to [http://www.fs.usda.gov/wps/portal/fsinternet/!ut/p/c5/04_SB8K8xLLM9MSSzPy8xBz9CP0os3gDfxMDT8MwRydLA1cj72BTSw8jAwgAykeaxcN4jhYG_h4eYX5hPgYwefy6w0H24dcPNgEHcDTQ9_PIz03VL8iNMMgycVQEAIzTHkw!/dl3/d3/L2dJQSEvUUt3QS9ZQnZ3LzZfME80MEkxVkFCOTBFMktTNUJIMjAwMDAwMDA!/?ss=110601&ttype=recarea&recid=72024&actid=42&navtype=BROWSEBYSUBJECT&navid=110190000000000&pnavid=110000000000000&pname=Central+Oregon+-+Devils+Lake Devil's Lake].


Alternative plan:

If starting before noon, and the weather coorperates and well prepred for backpacking and camping at high elevation (lightweight personal tent, sleeping pad, sleeping bag, canister, cookware cup, headlight), hike about 3 hours to camp at the [http://www.oregonhiking.com/oregon-adventures/100-hikes-in-the-central-oregon-cascades/hikes-in-the-three-sisters-area/moraine-lake-and-south-sister-hike Moraine Lake] campground. This saves 2+ hours of climbing time and about 500+ meters of elevation gain from the hike to South Sister summit.

* Day 2

Get up early.

Hike 6 mi to the summit.  Lunch break at the summit or Moraine Lake (stop #6 in <http://www.everytrail.com/guide/south-sister-summit>)

Hike 6 mi to the Devil's Lake campground (or back to Morain Lake campground).

Depending on time, the energy of people, camp the night in the area or drive home.

* Tentative for Day 3

Depending on what people want to do and if camped in the area on day 2 night, consider hiking the Green Lake trail (~ 9.5 mi, [https://www.flickr.com/photos/lumeng/sets/72157646716526968 pictures], and return by the end of day or camp near Green Lake (6000+ feet altitude) through night of day 3 and return midday on day 4.

[http://www.fs.usda.gov/recarea/deschutes/recreation/recarea/?recid=38872 Green Lakes/Soda Creek Trailhead]

[http://www.fs.usda.gov/wps/portal/fsinternet/!ut/p/c4/04_SB8K8xLLM9MSSzPy8xBz9CP0os3gDfxMDT8MwRydLA1cj72BTJw8jAwjQL8h2VAQAzHJMsQ!!/?ss=110601&ttype=recarea&recid=38870&actid=50&navtype=BROWSEBYSUBJECT&position=BROWSEBYSUBJECT&navid=110290000000000&pnavid=110000000000000&pname=Central+Oregon+-+Green+Lakes+Trail Green Lakes Trail]

== Tools and supplies ==

* rei.com, ''Mountaineering Checklist'', <http://www.rei.com/learn/expert-advice/mountaineering-checklist.html>

* Cam “Swami” Honan (a well known long-distance backpacker), http://www.thehikinglife.com/2014/12/pacific-crest-continental-divide-trail-gear-list/

Essential items:

* Hiking boot (perhaps similar to <http://www.amazon.com/Merrell-Mens-Ventilator-Hiking-Walnut/dp/B000KR1D1S/>).

* Gaiter, perhaps similar to <http://www.amazon.com/Outdoor-Research-Spark-Plug-Gaiters/dp/B00HPWSSYG/>. Additional info about gaiter: <http://www.outdoorresearch.com/blog/gear-geek/gaiters-what-are-they-and-what-are-different-types-for>.

* Trekking pole. (I have one extra trekking pole that can be lended.)

* A coat that can cover you when it's windy/chilly/rainy.

* An insulation layer that can keep you warm such as a down/fleece vest that can keep you warm at summit and at night.

* Sleeping bag if camping.

(I have a foam sleeping pad that I can lend out.)

* A cookware cup and fork/spoon. (I will have gas canister for cooking.)

* headlight and battery

Recommended items:

* Water bladder that you can carry in your backpack comfortably for many hours of hiking.

* Portable water filter, perhaps similar to <http://www.amazon.com/LifeStraw-LSPHF017-Personal-Water-Filter/dp/B006QF3TW4/>.  There are many accesses to crystal clear mountain creeks which you can enjoy with added ease of mind using a filter.

* Gloves (for [https://en.wikipedia.org/wiki/Scrambling scrambling] on the scree area composed of volcanic ash near the summit, which takes about 1 hr and about 1 mile of scrambling.

== Weather forecast ==

* <http://www.mountain-forecast.com/peaks/South-Sister-Volcano/forecasts/1500>

 If there is a cloud covering the summit, we will abort the climb to avoid the possibility of being trapped in some very adverse conditions.

* [http://volcanoes.usgs.gov/observatories/cvo/ Cascades Volcano Observatory - Volcano Hazards Program], http://volcanoes.usgs.gov/observatories/cvo/. See also [http://volcanoes.usgs.gov/activity/alertsystem/ USGS Volcanic Activity Alert-Notification System].

* [https://pnsn.org/ Pacific Northwest Seismic Network]

== Additional places of interest in the area ==

(Place names and recommendation scores.)

* Olympic National Park (9)
* Canon Beach (8)
* Portland
** Portland Art Museum (7)
** Evergreen Aviation & Space Museum (7)
* Mt. Hood
** Elk Meadow trail
* Silver Falls State Park (8)
* South Sister (9)
* Ona Beach State Park and Seal Rock State Park (Pacific beach) (7)
* Crater Lake National Park (8)

== (Optional) additional reference readings ==

* USDA Forest Service, ''Devils Lake/South Sister Trailhead'', <http://www.fs.usda.gov/recarea/deschutes/recarea/?recid=38850>

* traditionalmountaineering.org, ''A sample climb prospectus south sister by the south ridge in the summer'', <http://www.traditionalmountaineering.org/FAQ_ClimbingSouthSister.htm> (~40 min reading)

* oregonlive.com, ''South Sister climb deserves more respect'', <http://www.oregonlive.com/outdoors/index.ssf/2009/10/south_sister_climb_can_be_unde.html> (~15 min reading)

* statesmanjournal.com, ''Oregon's beginner-friendly mountain a grueling adventure'', <http://www.statesmanjournal.com/story/travel/outdoors/hikes/2014/09/03/oregons-beginner-friendly-mountain-grueling-adventure/15033805/> (~10 min reading)


=== Bear ===

* centerforwildlifeinformation.org, Be Bear Aware - Hiking and Camping, http://www.centerforwildlifeinformation.org/BeBearAware/Hiking_and_Camping/hiking_and_camping.html

* http://www.dfw.state.or.us/wildlife/living_with/black_bears.asp

* [http://www.dfw.state.or.us/wildlife/living_with/docs/BlkBearBroch.pdf ''Oregon is Black Bear Country'' booklet]

* [http://www.nps.gov/yell/planyourvisit/bearsafety.htm, National Park Service > Yellow Stone National Park > Bear Safety], http://www.nps.gov/yell/planyourvisit/bearsafety.htm

* [http://www.nps.gov/grsm/learn/nature/black-bears.htm National Park Service > Great Smoky Mountains National Park > Black Bears], http://www.nps.gov/grsm/learn/nature/black-bears.htm

=== Hiking tips ===

* Barney Scout Mann, Hike Like a Pro (general hiking tips), <http://www.backpacker.com/skills/ultralight/hike-like-a-pro/> (~20 min reading)

* rei.com, climbing articles, <http://www.rei.com/learn/expert-advice/climbing.html>

=== Region geography ===

* USDA Forest Service, ''Cascade Lakes Scenic Byway'' booklet, <http://www.visitbend.com/cascade-lakes-byway-map.pdf>

* Wikipedia, [https://en.wikipedia.org/wiki/Three_Sisters_(Oregon) ''Three Sisters''], <https://en.wikipedia.org/wiki/Three_Sisters_(Oregon)>

=== Geological information ===

* US Geological Survey, [http://volcanoes.usgs.gov/volcanoes/three_sisters/geo_hist_south_sister.html ''Geology and History Summary for Three Sisters''], <http://volcanoes.usgs.gov/volcanoes/three_sisters/geo_hist_south_sister.html>

* US Geological Survey, [http://volcanoes.usgs.gov/volcanoes/three_sisters/three_sisters_geo_hist_128.html Eruption History for South Sister], http://volcanoes.usgs.gov/volcanoes/three_sisters/three_sisters_geo_hist_128.html

* volcanocafe.wordpress.com, [https://volcanocafe.wordpress.com/2013/11/06/the-volcanoes-of-the-three-sisters-area-oregon/ The Volcanoes of the Three Sisters Area, Oregon], 2013, https://volcanocafe.wordpress.com/2013/11/06/the-volcanoes-of-the-three-sisters-area-oregon/

* BBC, [http://www.bbc.com/news/world-asia-29406211 Survivor's recount of Mt. Ontake (Japan) eruption]

* The Guardian, [http://www.theguardian.com/world/2014/sep/29/japanese-volcano-eruption-survivors-heartbreaking-stories-victims, Japanese volcano eruption survivors tell heartbreaking stories of victims], Sept. 2014, http://www.theguardian.com/world/2014/sep/29/japanese-volcano-eruption-survivors-heartbreaking-stories-victims

[[Category:Travel]]
[[Category:2015]]
